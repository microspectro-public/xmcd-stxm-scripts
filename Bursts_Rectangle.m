% Rectangular Pulse Generation
% - Phillip Wohlhueter (phillip.wohlhueter@psi.ch)
clear all
close all
clc

%EDIT PARAMETERS HERE
% amplidtudes in dBm -> set dBm = 1 for amplitudes in Vpp -> dBm = 0
dBm = 0;

%amplitude for continuous-wave excitation
ampl_con = 0;
% set burst amplitude [V]
ampl_burst = 0.25;

M = 10; % CAN BE: 1,2,4,5,10,20 -- RESOLUTION: 2/M [ns]
N = 137;

% number of bursts
n_bursts = M;

% length of file in ns (should always be MORE than 96!)
length = N*2;
% number of channels used
channels = N;

% pulse width in ns
pulse_w = 5;

% set time resolution in ns
tr = 2/M;
%tr = 2/(n_bursts);

% enable dipolar pulses (0 = off, 1 = on)
bipolar = 0;
separation = 0; % Time separation in ns

% sampling rate in Gs
sample_rate = 10;

% save = 1: save yes
% save = 0: no save
save = 1;

%--------------------------------------------------------------------------
% set directory
directory = 'Z:\Excitation files';
cd(directory);

% pulse spacing in points
ch_spacing = floor(channels/n_bursts);
p_space = (ch_spacing*2-tr)*sample_rate;

% time interval between points
dt = 1/(sample_rate*1e9);
% total number of points
n_points = length*sample_rate;

% convert amplitudes into V(pp)
if dBm == 0
    a_con = ampl_con*2;
    a_burst = ampl_burst*2;
else
    a_con = sqrt(1E-3*50)*10^(ampl_con/20)*sqrt(2)*4/30;
    a_burst = sqrt(1E-3*50)*10^(ampl_burst/20)*sqrt(2)*4/30;
end

% amplitude vector
a = zeros(1,n_points);
if ampl_con > 0
    for i = 1:n_points
        a = a_con;
    end
end
for j = 0:n_bursts-1
    a(1,j*p_space+1) = a_burst./4;
    a(1,j*p_space+2) = a_burst./2;
    a(1,j*p_space+pulse_w*sample_rate) = a_burst./4;
    a(1,j*p_space+pulse_w*sample_rate-1) = a_burst./2;
    if bipolar == 1
        a(1,j*p_space+pulse_w*sample_rate+1+sample_rate*separation) = -a_burst./4;
        a(1,j*p_space+pulse_w*sample_rate+2+sample_rate*separation) = -a_burst./2;
        a(1,j*p_space+pulse_w*2*sample_rate+sample_rate*separation) = -a_burst./4;
        a(1,j*p_space+pulse_w*2*sample_rate-1+sample_rate*separation) = -a_burst./2;
    end
    for k = 1:pulse_w*sample_rate-4
        a(1,j*p_space+k+2) = a_burst;
        if bipolar == 1
            a(1,j*p_space+pulse_w*sample_rate+k+2+sample_rate*separation) = -a_burst;
        end
    end
end

a = a';

if save == 1
    if bipolar == 1
        if dBm == 0
            dlmwrite([num2str(n_bursts),'_bipolar_rectangular_bursts_at_',num2str(ampl_burst),'Vpp_',num2str(pulse_w),'ns.txt'], a, '\t');
        else
            dlmwrite([num2str(n_bursts),'_bipolar_rectangular_bursts_at_',num2str(ampl_burst),'dBm_',num2str(pulse_w),'ns.txt'], a, '\t');
        end
    else
        if dBm == 0
            dlmwrite([num2str(n_bursts),'_rectangular_bursts_at_',num2str(ampl_burst),'V_',num2str(pulse_w),'ns.txt'], a, '\t');
        else
            dlmwrite([num2str(n_bursts),'_rectangular_bursts_at_',num2str(ampl_burst),'dBm_',num2str(pulse_w),'ns.txt'], a, '\t');
        end
    end
end

% set name for saving
if bipolar == 1
    if dBm == 0
        plot_savename = [num2str(directory),'\',num2str(n_bursts),'_bipolar_rectangular_bursts_at_',num2str(ampl_burst),'V_',num2str(pulse_w),'ns.png'];
    else
        plot_savename = [num2str(directory),'\',num2str(n_bursts),'_bipolar_rectangular_bursts_at_',num2str(ampl_burst),'dBm_',num2str(pulse_w),'ns.png'];
    end
else
    if dBm == 0
        plot_savename = [num2str(directory),'\',num2str(n_bursts),'_rectangular_bursts_at_',num2str(ampl_burst),'V_',num2str(pulse_w),'ns.png'];
    else
        plot_savename = [num2str(directory),'\',num2str(n_bursts),'_rectangular_bursts_at_',num2str(ampl_burst),'dBm_',num2str(pulse_w),'ns.png'];
    end
end
% plot data
h = figure;
plot(a)
if bipolar == 1
    if dBm == 0
        title([num2str(n_bursts),' bipolar rectangular bursts @ ',num2str(ampl_burst),' V, ',num2str(pulse_w),' ns']);
    else
        title([num2str(n_bursts),' bipolar rectangular bursts @ ',num2str(ampl_burst),' dBm, ',num2str(pulse_w),' ns']);
    end
else
    if dBm == 0
        title([num2str(n_bursts),' rectangular bursts @ ',num2str(ampl_burst),' V, ',num2str(pulse_w),' ns']);
    else
        title([num2str(n_bursts),' rectangular bursts @ ',num2str(ampl_burst),' dBm, ',num2str(pulse_w),' ns']);
    end
end
xlabel('Time [0.1ns]');
ylabel('Amplitude [V]');
if save == 1
    saveas(h,plot_savename,'png');
end
