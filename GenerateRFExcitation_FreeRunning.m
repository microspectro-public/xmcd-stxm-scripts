close all
clear all

%% User parameters

f = 215*1e6; % Frequency of excitation [Hz]
A = 1;   % Amplitude of excitation [V]
phase = 0;  % Phase of excitation [deg]
savepath = 'Z:\Excitation files\';

%% Creation of excitation
minimumLength = 120e-9;
markerLength = 1e-9;
samplingFrequency = 64e9;

maximumPhaseOffset = .5; % Max phase offset when sampling [0.5 degrees]

period = 1/f;

R = ceil(minimumLength/period);

totalLength = R*period;

totalLengthResampled = ceil(totalLength*samplingFrequency)/samplingFrequency;

phaseOffset = (totalLengthResampled-totalLength)/period*180;

while phaseOffset > maximumPhaseOffset
    R = R+1;
    totalLength = R*period;
    totalLengthResampled = ceil(totalLength*samplingFrequency)/samplingFrequency;
    phaseOffset = (totalLengthResampled-totalLength)/period*180;
    
end

fprintf('>>>> Phase offset of excitation at end is >>>> %f degrees <<<<\n',phaseOffset);
fprintf('>>>> Number of periods in excitation file >>>> %d <<<<\n',R);
fprintf('>>>> Period in ns >>>> %f <<<<\n',R*period*1e9);

numberOfPointsResampled = ceil(totalLength*samplingFrequency);
signal = zeros(numberOfPointsResampled,2);

for i=1:size(signal,1)
    
    if i/samplingFrequency < markerLength
        signal(i,2) = 1;
    end
    
    signal(i,1) = A*sin(((i/samplingFrequency)*f+phase/360)*2*pi);
end

% Resampling the wave so that it is actually loaded by the AWG (must be a
% multiple of 256)

RAWG = lcm(size(signal,1),256)/size(signal,1);

if RAWG > 100
    error("RAWG too high (%d). Change length of file",RAWG);
end

signal_final = signal;

while RAWG > 1
    signal_final = [signal_final; signal];
    RAWG = RAWG - 1;
end
    
clear signal

savename = strcat(savepath,'RF_f_',num2str(f),'_A_',num2str(A),'_phase_',num2str(phase),'_withMKR_Keysight.txt');

dlmwrite(savename, signal_final);

t = [0:1/samplingFrequency:1/samplingFrequency*(size(signal_final,1)-1)];

plot(t, signal_final(:,1));
hold on; 
plot(t, signal_final(:,2),'r')