% Waveform Generation
% - Stephanie Stevenson (stephanie.stevenson@psi.ch)
% - Phillip Wohlh�ter (phillip.wohlhueter@psi.ch)
clear all
close all
clc

%EDIT PARAMETERS HERE
% amplidtudes in dBm -> set dBm = 1; for amplitudes in Vpp -> dBm = 0
dBm = 0;

%amplitude for continuous-wave excitation
ampl_con = 0.6;
% set phase shift in fraction of pi
phase_shift = 0;

% save = 1: save yes
% save = 0: no save
save = 1;

N = 53;
M = 80;

% length of file in ns (should always be MORE than 96!)
length = N*2;
% EDIT FREQUENCIES HERE
% base frequency
%fcw = 500/212*108;
fcw = 500./N*M;
%--------------------------------------------------------------------------
% set directory
directory = 'Z:\excitation files';
cd(directory);

% Define continuous-wave frequency
f_cw = fcw*1e6;

% convert amplitudes into V(pp)
if dBm == 0
    a_cw = ampl_con;
else
    a_cw = sqrt(1E-3*50)*10^(ampl_con/20)*sqrt(2)*4/30;
end

T_cw = 1/f_cw;
dt = 1/10E9;                      % Defines sampling rate, 10 Gs
n_points = length*10;                         % Number of points in final file
nrcycles = n_points/T_cw*1e-10;     % Calculate correct number of cycles to match number of points

% calculate phase shift
shift_t = T_cw*phase_shift/2;

% time length of cw and burst
tmin_cw = shift_t;
tmax_cw = nrcycles*T_cw-dt+shift_t;

% number of points for cw and burst
t_cw = (tmin_cw:dt:tmax_cw);

[k,l_tcw] = size(t_cw);

% correct if length of cw and burst do not add up to required number of
% points
if l_tcw == n_points-1
    tmax_cw = nrcycles*T_cw;
    t_cw = (tmin_cw:dt:tmax_cw);
end

% Sinusoidal continuous-wave excitation
x_cw = a_cw*sin(2*pi*(f_cw*t_cw));


DS = transpose([t_cw;x_cw]);

[time,amp] = size(DS);

if time-n_points == 0
    % Name the .txt file, import to AWG from Scratch Drive
    % On AWG: File > Import from File... > Text > Format: Analog, Normalize: None
    f_cw_save = round(f_cw/1e4)/100;
    
    if phase_shift == 0
        if dBm == 0
            savename = [num2str(f_cw_save),'MHz_CW_',num2str(ampl_con),'Vpp.txt'];
            savetitle = [num2str(f_cw_save),'MHz CW, ',num2str(ampl_con),'Vpp'];
        else
            savename = [num2str(f_cw_save),'MHz_CW_',num2str(dbm_cw),'dBm.txt'];
            savetitle = [num2str(f_cw_save),'MHz CW, ',num2str(dbm_cw),'dBm'];
        end
    else
        if dBm == 0
            savename = [num2str(f_cw_save),'MHz_CW_',num2str(ampl_con),'Vpp_',num2str(phase_shift),'pi_phase_shift.txt'];
            savetitle = [num2str(f_cw_save),'MHz CW, ',num2str(ampl_con),'Vpp, ',num2str(phase_shift),'pi phase shift'];
        else
            savename = [num2str(f_cw_save),'MHz_CW_',num2str(dbm_cw),'dBm_',num2str(phase_shift),'pi_phase_shift.txt'];
            savetitle = [num2str(f_cw_save),'MHz CW, ',num2str(dbm_cw),'dBm, ',num2str(phase_shift),'pi phase shift'];
        end
    end
    
    if save == 1
        dlmwrite(savename, DS(:,2), '\t');
    end
    
    % set name for saving
    plot_savename = [num2str(directory),'\',num2str(savename),'.png'];
    
    % plot data
    h = figure;
    plot(DS)
    hold on
    b = DS(1:20:end,2);
    plot([1:20:size(DS,1)],b,'*r');
    title(savetitle);
    xlabel('Time [0.1ns]');
    ylabel('Amplitude [Vpp]');
    if save == 1
        saveas(h,plot_savename,'png');
    end
    
    disp('Everything is fine');
else
    disp('Number of points does NOT match file length!');
end

newIndex = zeros(1,N);

for i=1:N
    newIndex(i) = (i/N*M-floor(i/N*M))*N+1;
    phase(i) =(i/N*M*360)-floor(i/N*M)*360;
end

uint16(newIndex)

for i=1:N
    b_reconstructed(uint16(newIndex(i))) = b(i);
end

figure(2)
plot([1:1:size(b_reconstructed,2)]*2/M,b_reconstructed,'r*');
hold on
plot([1:1:size(b_reconstructed,2)]*2/M,b_reconstructed,'b-');
xlabel('Time [ns]');
ylabel('Amplitude [V]');
title('Reconstructed Signal');
