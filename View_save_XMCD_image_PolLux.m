% This script will read two .hdf5 images of opposite helicities generated
% by the STXM contorl software. Matlab R2006a or newer required (bug tested
% in R2015a).
% 
% The script, after loading the images, will save each helicity and the
% calculated XMCD image in 8-bit .tiff format in a folder named "Analyzed",
% which will be created in the same folder as the one in which the image 
% will be loaded from.
%
% The script also has implemented an automatic drift correction algorithm,
% which calculates the cross correlation between the two images (after
% passing through a prewitt edge filter). This gives the user a suggestion
% on which drift correction they should use (results may vary a lot!)
%
% For bugs/improvements, please contact S. Finizio (simone.finizio@psi.ch)
% Feel free to edit/improve the script as it best fits you

function [I_XMCD, firstImagePathComplete, secondImagePathComplete] = View_save_XMCD_image_PolLux(date, image1,image2)

    %% USER INPUT

    % Here start editing

    % Location of the image to load
    experimentPath = 'Z:\Data1\'; % Here insert the path to the experimental folder
     % Here insert the date of the image (in yyyy-mm-dd format)
    
    if nargin == 0
        
        date = '2019-09-08';
        firstImageNumber = '244'; % Here insert the number of the image (3 digits)
        secondImageNumber = '245'; % Here insert the number of the image (3 digits)
        
    else
        if image1 < 10
           firstImageNumber = strcat('00',num2str(image1));
        elseif image1 < 100
           firstImageNumber = strcat('0',num2str(image1));
        else
           firstImageNumber = num2str(image1); 
        end
        
       if image2 < 10
           secondImageNumber = strcat('00',num2str(image2));            
        elseif image2 < 100
           secondImageNumber = strcat('0',num2str(image2));
        else
           secondImageNumber = num2str(image2); 
        end
    end
    entryNumber = 'entry1'; % In case of regional scans, here insert the entry number

    % Detector to use
    detector = 2; % 1 = "Analog 0"; 2 = "Counter 0"; 3 = "PHC"

    % Drift correction
    driftX = 0;
    driftY = 0;
    suggestDrift = 0; % Set to 0 if you do not want the computer to suggest you a value for the drift correction
    n_int = 4;

    % User choices
    saveImage = 1; % Set to 0 if you simply want to see the image without saving it (will re-scale the image between 0 and 1)
    showImage = 0; % Set to 0 if you don't want to see the image (just saving it)
    autoContrast = 1; % Set to 1 if you want the computer to automatically adjust the contrast for you
    
    % Correction for positioning errors
    correct_data = 1; % Set to 1 if you want to correct the image using the measured sample position

    % Here end editing (leave the rest of the code as-is)

    %% INPUT CONTROL
    % Here we control that the input is correct

    firstImagePathComplete = strcat(experimentPath,date,'\Sample_Image_',date,'_',firstImageNumber,'.hdf5'); % Generating the final path to the image
    secondImagePathComplete = strcat(experimentPath,date,'\Sample_Image_',date,'_',secondImageNumber,'.hdf5'); % Generating the final path to the image

    % Checks if both files are there
    if exist(firstImagePathComplete,'file') == 0 
        error(strcat('The image ',firstImagePathComplete,' does NOT exist. Did you synchronize?'));
    end
    if exist(secondImagePathComplete,'file') == 0
        error(strcat('The image ',secondImagePathComplete,' does NOT exist. Did you synchronize?'));
    end

    % Here we get the version of Matlab (the command changes between versions)
    currVersion = version('-release');

    if str2double(currVersion(1:4)) >= 2009
        command = 'h5';
    else
        command = 'hdf5';
    end

    % Checks that the helicities are correctly selected
    % First we try with the orbit bumps

    dataset = strcat('/',entryNumber,'/collection/ring_y_asym/value'); % This looks at the asymmetry of the y position of the beam
    firstHelicity = eval(strcat(command,'read(firstImagePathComplete,dataset)'));
    secondHelicity = eval(strcat(command,'read(secondImagePathComplete,dataset)'));

    % Here we look at which image has the positive/negative helicity

    if firstHelicity > secondHelicity
        xmcdOrder = {'first', 'second'};
    elseif secondHelicity > firstHelicity
        xmcdOrder = {'second', 'first'};
    else % if both helicities are the same
        error('Both helicities are the same');
    end


    % Here we look if one of the two images has linear light
    if firstHelicity == 0 | secondHelicity == 0
        fprintf('Warning: One of the two images was done at linear polarization\n');
    end

    if saveImage == 0 & showImage == 0
        fprintf('Warning: The image will neither be shown nor be saved\n');
    end

    %% READ IMAGE

    % Here we define the dataset
    for i=1:1 % Horrible trick to make the break function work in the switch
        switch detector
            case 1
                detectorPath = '/analog0/';
                break;
            case 2
                detectorPath = '/counter0/';
                break;
            case 3
                detectorPath = '/PHC/';
                break;
            otherwise
                error('Not a suitable detector selected');
        end
    end

    dataset = strcat('/',entryNumber,detectorPath,'data');

    try % Checks that everything is working fine
        temp = eval(strcat(command,'info(firstImagePathComplete,dataset)')); 
        temp = eval(strcat(command,'info(secondImagePathComplete,dataset)')); 
    catch
        error('Not a suitable detector selected');
    end

    clear temp

    % Reads the images

first_string = char(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset)'));
second_string = char(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset)'));

I_pos = double(eval(first_string));
I_neg = double(eval(second_string));

% Correct for the non-regular grid

if correct_data

    dataset_x = strcat('/entry1/instrument/sample_x/data');
    dataset_y = strcat('/entry1/instrument/sample_y/data');
    dataset_data = strcat('/entry1/instrument',detectorPath,'data');
    dataset_setpx = strcat('/entry1',detectorPath,'sample_x');
    dataset_setpy = strcat('/entry1',detectorPath,'sample_y');


    x_points = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_x)')));
    y_points = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_y)')));
    data_points = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_data)')));
    setpx = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_setpx)')));
    setpy = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_setpy)')));
    

     Xq = reshape(x_points, size(I_pos,1), size(I_pos,2));
     Yq = reshape(y_points, size(I_pos,1), size(I_pos,2));
     V = reshape(data_points, size(I_pos,1), size(I_pos,2));
    [X,Y] = meshgrid(setpx,setpy);
    Xq = Xq-mean(mean(Xq));
    Yq = Yq-mean(mean(Yq));
    X = X-mean(mean(X));
    Y = Y-mean(mean(Y));
    
    I_pos = griddata(Xq,Yq,V,X,Y);

    x_points = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_x)')));
    y_points = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_y)')));
    data_points = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_data)')));
    setpx = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_setpx)')));
    setpy = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_setpy)')));


     Xq = reshape(x_points, size(I_neg,1), size(I_neg,2));
     Yq = reshape(y_points, size(I_neg,1), size(I_neg,2));
     V = reshape(data_points, size(I_neg,1), size(I_neg,2));
    [X,Y] = meshgrid(setpx,setpy);
    Xq = Xq-mean(mean(Xq));
    Yq = Yq-mean(mean(Yq));
    X = X-mean(mean(X));
    Y = Y-mean(mean(Y));
    
    I_neg = griddata(Xq,Yq,V,X,Y);
    
    I_pos = flipdim(I_pos,1);
    I_neg = flipdim(I_neg,1);
else
    I_pos = imrotate(I_pos,90);
    I_neg = imrotate(I_neg,90);

end

    %% CALCULATE XMCD

    % Enlargement of images (if needed -- uses CUBIC interpolation)
    I_reg = zeros(size(I_pos));

    if n_int == 1
        I_reg = circshift(I_neg,[-driftY -driftX]);
    else
        dx = driftX;
        dy = driftY;
        driftX = dy / n_int;
        driftY = dx / n_int;


        for j=1:size(I_neg,1)
            for k=1:size(I_neg,2)

                j_cur = j+floor(driftX); % ROW --> A(row, col)
                k_cur = k+floor(driftY); % COLUMN

                if (j_cur < 1 || j_cur+1 > size(I_neg,1)) || (k_cur < 1 || k_cur+1 > size(I_neg,2))
                    I_reg(j,k) = I_neg(1,1);
                else
                    int_1 = I_neg(j_cur,k_cur) + (I_neg(j_cur+1,k_cur)-I_neg(j_cur,k_cur))*(driftX-floor(driftX));
                    int_2 = I_neg(j_cur,k_cur+1) + (I_neg(j_cur+1,k_cur+1)-I_neg(j_cur,k_cur+1))*(driftX-floor(driftX));
                    int_3 = int_1 + (int_2 - int_1)*(driftY-floor(driftY));
                    I_reg(j,k) = int_3;
                end
            end
        end

        driftX = dx/n_int;
        driftY = dy/n_int;

        clear dx dy
    end

    I_neg = I_reg;


    % Drift correction (corrects the drift of the SECOND image)
    if suggestDrift
        
        I_show = double(I_pos);
        I_show = I_show-min(min(I_show(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_show = I_show/max(max(I_show(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        imshow((I_show));
        h = imrect;
        position = wait(h);
        
        drifts = dftregistration(fft2(imcrop(I_pos,position)),fft2(imcrop(I_neg,position)),n_int);

        drift(1,2) = drifts(3);
        drift(2,2) = drifts(4);
        if n_int == 1
            I_reg = circshift(I_neg,[drift(1,2) drift(2,2)]);
        else
            for j=1:size(I_neg,1)
                for k=1:size(I_neg,2)

                    j_cur = j-floor(drift(1,2)); % ROW --> A(row, col)
                    k_cur = k-floor(drift(2,2)); % COLUMN

                    if (j_cur < 1 || j_cur+1 > size(I_neg,1)) || (k_cur < 1 || k_cur+1 > size(I_neg,2))
                        I_reg(j,k) = I_neg(1,1);
                    else
                        int_1 = I_neg(j_cur,k_cur) + (I_neg(j_cur+1,k_cur)-I_neg(j_cur,k_cur))*(drift(1,2)-floor(drift(1,2)));
                        int_2 = I_neg(j_cur,k_cur+1) + (I_neg(j_cur+1,k_cur+1)-I_neg(j_cur,k_cur+1))*(drift(1,2)-floor(drift(1,2)));
                        int_3 = int_1 + (int_2 - int_1)*(drift(2,2)-floor(drift(2,2)));
                        I_reg(j,k) = int_3;
                    end
                end
            end
        end
        I_neg = I_reg;
    else
%        I_neg = circshift(I_neg,[-driftY,driftX]);
    end

        
    % Calculation of XMCD image
    I_XMCD = (I_pos-I_neg)./(I_pos+I_neg); % xmcd = difference/sum
    I_sum = (I_pos+I_neg);

    %% SHOW AND SAVE IMAGE

    % Contrast correction
    I_pos = double(I_pos);
    I_neg = double(I_neg);
    % I_XMCD is already in double format from the division

    if autoContrast & showImage
        I_pos = I_pos-min(min(I_pos(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_pos = I_pos/max(max(I_pos(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_neg = I_neg-min(min(I_neg(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_neg = I_neg/max(max(I_neg(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_XMCD = I_XMCD-min(min(I_XMCD(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_XMCD = I_XMCD/max(max(I_XMCD(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
    end

    if showImage
        figure(1)
        imshow(I_pos);
        figure(2)
        imshow(I_neg);
        figure(3)
        imshow(I_XMCD);
    end

    if saveImage
        savePath = strcat(experimentPath,date,'\Analysed\');
        % Creates the save folder if it does not exist
        if ~exist(savePath,'dir')
            mkdir(savePath);
            fprintf('Warning: Save folder did not exist - it was now created for you\n');
        end
        % Normalization of the image between 0 and 1
        I_XMCD = I_XMCD-min(min(I_XMCD(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_XMCD = I_XMCD./max(max(I_XMCD(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_XMCD = uint16(I_XMCD*(2^16));
        I_sum = I_sum-min(min(I_sum(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_sum = I_sum./max(max(I_sum(abs(ceil(driftY))+1:end-(abs(ceil(driftY))+1),abs(ceil(driftX))+1:end-(abs(ceil(driftX))+1))));
        I_sum = uint16(I_sum*(2^16));
        % Saving the image
        imwrite(I_XMCD,strcat(savePath,'XMCD_Image_',date,'_',firstImageNumber,'.tif'));
        imwrite(I_sum,strcat(savePath,'Sum_Image_',date,'_',firstImageNumber,'.tif'));
    end


    % end
    