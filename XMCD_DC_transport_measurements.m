close all
clear all

%% USER INPUT

debug = 1;

% Path for finding the images
root_path = 'Z:\Data1\';

% Dates and range for the images
dates = {'2017-06-09'};
image_range = {[300 341]};

% Precision in 1/x px for the drift correction
precision = 100;

% Edge filtering to be used for the drift correction
useFilter = 1; % Set to 1 if you want to use edge filtering
filterType = 'sobel'; % Available options: 'sobel', 'canny', 'log', 'prewitt', 'roberts'
threshold = 0.1;


%% Select ROI for registration

I_template = View_save_single_image_PolLux(dates{1},image_range{1}(1));

I_show = double(I_template);
I_show = I_show-min(min(I_show(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
I_show = I_show/max(max(I_show(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
imshow((I_show));
h = imrect;
position = wait(h);

if debug & useFilter
    imshow(edge(imcrop(I_show,position),filterType,'nothinning',threshold))
    pause
end


%% Registration 

counter = 1;
n_int = precision;

for i=1:length(dates)
    for j=image_range{i}(1):image_range{i}(2)
        
        I_to_register = double(View_save_single_image_PolLux(dates{i},j));
        I_to_register = I_to_register-min(min(I_to_register(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
        I_to_register = I_to_register/max(max(I_to_register(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
        I_template = I_show;
        
        if useFilter
            drifts(:,counter) = dftregistration(fft2(edge(imcrop(I_template,position),filterType,'nothinning',threshold)),fft2(edge(imcrop(I_to_register,position),filterType,'nothinning',threshold)),n_int);
        else
            drifts(:,counter) = dftregistration(fft2(imcrop(I_template,position)),fft2(imcrop(I_to_register,position)),n_int);
        end

        drift(1) = drifts(3,counter);
        drift(2) = drifts(4,counter);
        I_to_register = double(View_save_single_image_PolLux(dates{i},j));
        if n_int == 1
            I_reg = circshift(I_to_register,[drift(1) drift(2)]);
        else
            for j=1:size(I_to_register,1)
                for k=1:size(I_to_register,2)

                    j_cur = j-floor(drift(1)); % ROW --> A(row, col)
                    k_cur = k-floor(drift(2)); % COLUMN

                    if (j_cur < 1 || j_cur+1 > size(I_to_register,1)) || (k_cur < 1 || k_cur+1 > size(I_to_register,2))
                        I_reg(j,k) = I_to_register(1,1);
                    else
                        int_1 = I_to_register(j_cur,k_cur) + (I_to_register(j_cur+1,k_cur)-I_to_register(j_cur,k_cur))*(drift(1)-floor(drift(1)));
                        int_2 = I_to_register(j_cur,k_cur+1) + (I_to_register(j_cur+1,k_cur+1)-I_to_register(j_cur,k_cur+1))*(drift(1)-floor(drift(1)));
                        int_3 = int_1 + (int_2 - int_1)*(drift(2)-floor(drift(2)));
                        I_reg(j,k) = int_3;
                    end
                end
            end
        end
        I_registered(:,:,counter) = I_reg;
        counter = counter+1;
    end
end

%% XMCD (always [1-2-1-2])

I_XMCD = zeros(size(I_registered,1),size(I_registered,2),size(I_registered,3)/2);

for i=1:size(I_registered,3)/2
    
    I_dif(:,:,i) = (I_registered(:,:,2*i-1)-I_registered(:,:,2*i));
    I_sum(:,:,i) = (I_registered(:,:,2*i-1)+I_registered(:,:,2*i));
    
    I_XMCD(:,:,i) = (I_registered(:,:,2*i-1)-I_registered(:,:,2*i))./((I_registered(:,:,2*i-1)+I_registered(:,:,2*i)));
    
end

if debug
   implay(I_XMCD)
   pause
end

%% Definition of elliptical mask for transport measurements

I_show = double(I_XMCD(:,:,1));
I_show = I_show-min(min(I_show(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
I_show = I_show/max(max(I_show(abs(ceil(10))+1:end-(abs(ceil(10))+1),abs(ceil(10))+1:end-(abs(ceil(10))+1))));
imshow(I_show);
h = imellipse;
position = wait(h);

mask = poly2mask(position(:,1),position(:,2),size(I_show,1),size(I_show,2));

%%

